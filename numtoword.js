//Names of digits that will be used for answer
const oneName = ["","one","two","three","four","five","six","seven","eight","nine"]
const tenName = ["","ten","twenty","thirty", "forty","fifty","sixty","seventy","eighty","ninety"];
const teenName = ["","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"];
const hundredName = ["","one hundred","two hundred","three hundred","four hundred","five hundred","six hundred","seven hundred","eight hundred","nine hundred"]

//creating function that will convert numbers to words
function numToW() {
  let num = document.getElementById("textInput").value; //assigning num to the user input on page
  //var numString = num.toString(); //creating a variable to represent the strign form of the user input on page
  

  for (let i = 0; i <num.length; i++) { //repeat this for loop for the entire length of the user input
    }if (num.length === 1) { //if there is only one character in the input number, write the word version of that to document
      document.write(oneName[num])
    } else if (num.length === 2 && num[0] === "1" && num[1] === "0") { //if there are two characters in user input, and the first one is a 1, return the word form of the input by obtaining from arrays
      document.write(tenName[1]);
    } else if (num.length === 2 && num[0] === "1") { //if there are two characters in user input, and the first one is a 1, return the word form of the input by obtaining from arrays
      document.write(teenName[num[1]]);
    } else if (num.length === 2 && num[0] !== "1") { // if there are two characters in the user input, and the user input is not 11-19
      document.write(tenName[num[0]] + ' ' + oneName[num[1]]);
    } else if (num.length === 3 && num[1] === '1') { //if there are two characters in user input, and the first one is a 1, return the word form of the input by obtaining from arrays
      document.write(hundredName[num[0]] + ' ' + teenName[num[2]])
    } else if (num.length === 3 && num[1] !== 1) { //if there are three characters in the user input, meaning it is in the hundred's
      document.write(hundredName[num[0]] + ' ' + tenName[num[1]] + " " + oneName[num[2]]);
    }
}